#!/bin/bash

# Define scenarios
n_scenarios=5
cycles=(		10000	50000 	5000	100000	50000)
max_speed=(		50		20		40		30		20)
min_speed=(		1		5		20		5		10)
max_length=(	100		60		10		200		65)
min_length=(	10		40		2		100		25)
max_interval=(	30		120		5		90		90)
max_move_time=(	3		10		2		5		5)

# Run system
echo "This script will generate traces from toy-example/. It will vary several parameters, to obtain different traces. Recompiling will be done automatically. Output will be available in toy-example/traces/."
echo "" # new line

echo -e "\033[0;36mGenerating traces, please be patient...\033[0m"

for i in $( eval echo {1..$n_scenarios} )
do
	echo -e "\033[0;36mGenerating trace $i/$n_scenarios (size = ${cycles[i-1]}).\033[0m"
	cd toy-example

	# Update settings
	environment_defs="\-DTRAIN_MAX_SPEED=${max_speed[i-1]} \-DTRAIN_MIN_SPEED=${min_speed[i-1]} \-DTRAIN_MAX_LENGTH=${max_length[i-1]} \-DTRAIN_MIN_LENGTH=${min_length[i-1]} \-DTRAIN_MAX_INTERVAL=${max_interval[i-1]} \-DBARRIER_MAX_MOVE_TIME=${max_move_time[i-1]}"
	controller_defs="\-DTRACING \-DN_OF_CYCLES=${cycles[i-1]}"
	
	sed -i.old "s/DEFS:=.*$/DEFS:=$environment_defs/g" environment/Makefile
	sed -i.old "s/DEFS:=.*$/DEFS:=$controller_defs/g" controller/Makefile

	# Build and collect trace
	cmd="./tracing.sh trace_$i"
	$($cmd > /dev/null) # Remove system out, still shows system err
	echo ""

	cd ..
done

# Restore settings
sed -i.old "s/DEFS:=.*$/DEFS:=/g" toy-example/environment/Makefile
sed -i.old "s/DEFS:=.*$/DEFS:=-DTRACING -DDRAW_TRAIN/g" toy-example/controller/Makefile
