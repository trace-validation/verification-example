#!/bin/bash

echo "This script will take all JSON files in requirements/ and convert these to NBA specifications. For this it uses requirement-converter which it will build first. The output files will also be in requirements/."
echo "" # new line

echo "Building and packaging requirement-converter..."
cd requirement-converter
mvn -q clean install
cd ..
echo "done"	
echo "" # new line

for requirement in requirements/*.json
do
	if [ ${requirement: -9} == ".nba.json" ]
	then
		# Skip output files
		continue
	fi

	echo "Converting: $requirement"

	# Output to .nba.json file
	output=$(echo "$requirement" | sed "s/\.json/\.nba\.json/g")
	neg_output=$(dirname $output)/neg_$(basename $output)
	# Do conversion
	cmd="java -jar requirement-converter/target/requirement-converter-0.0.1-SNAPSHOT-jar-with-dependencies.jar $requirement $output $neg_output"
	echo "Executing $cmd"
	$($cmd > /dev/null) # Remove system out, still shows system err
	echo "Result in $output"
	echo ""
done
