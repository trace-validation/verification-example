#!/bin/bash

echo "This script will take all JSON files in visualisation/ and convert these to DOT representation. For this it uses the Buchi visualiser which it will build first. The output files will also be in visualisation/."
echo "" # new line

echo "Building and packaging requirement-converter..."
cd buchi-visualiser
mvn -q clean install
cd ..
echo "done"	
echo "" # new line

for nba in visualisation/*.nba.json
do
	echo "Converting: $nba"

	# Do conversion
	cmd="java -jar buchi-visualiser/target/buchi-visualiser-0.0.1-SNAPSHOT-jar-with-dependencies.jar $nba"
	echo "Executing $cmd"
	$($cmd > /dev/null) # Remove system out, still shows system err
	echo ""
done