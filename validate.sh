#!/bin/bash

echo "This script will take all NBA JSON files in validation/requirements/ and traces in validation/traces/. Each requirement is validated on each trace."
echo "" # new line

echo "Building and packaging trace-validator..."
cd trace-validator
mvn -q initialize
mvn -q clean install
cd ..
echo "done"	
echo "" # new line

for requirement in validation/requirements/*.json
do
	neg_requirement=
	
	# Skip negated NBAs
	if echo "$requirement" | grep -q neg_ ; then
		continue
	else 
		neg_requirement=$(dirname $requirement)/neg_$(basename $requirement)
	fi	
	
	for trace in validation/traces/trace_*; do
		echo -e "\033[0;36mChecking ${requirement: 24} on ${trace: 18}:\033[0m"

		# Validate
		java -jar trace-validator/target/trace-validater-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
				$requirement $neg_requirement $trace/ust/uid/1000/64-bit
		echo ""
	done
done
