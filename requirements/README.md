# Correct Requirements

## Barriers down
If sensor a is activated, a has to stay activated at least until barriers start moving down.
It is also accepted that the gate has already been moving down or is already down.
This has to happen before the end of the loop.

`G( a -> ( (a && !endloop) U ( (N going down || N down) && (S going down || S down) ) ) )`

## One train
There may not be more than one train on this section of rail.

`!G(a && !b && c)`

## Stop signal
If sensor b is activated and not both barriers are down, set the signal to stop.
All within a single iteration.

`G( (b && !(N down && S down)) -> ((b && !(N down && S down) && !endloop) U !signal))`

## Phase ordering
Check that after 'retrieve', there is a 'determine' in each iteration.

`G( (retrieve -> (retrieve U (!endloop && (!endloop U determine)))))`

## Go signal
Set signal to go if the signal is stop and both barriers are down.

`G ( (signal && N down && S down) U signal)`

# Problematic Requirements

## Uncovered scenario
If sensor c is activated, the barriers must be moving up or be up.

`G( c -> ((N going up || N up) && (S going up || S up) ) )`

**Problem:**
The first time c becomes true, there has been no time to start raising the barriers.
Therefore, the first time c becomes true, this requirement will fail.

## By construction
Wait for a 'false' event in the future.

`F(c && !c)`

**Problem:**
This is impossible to violate if updating actions happens properly and both c APs are the
same. The requirement will be accepted until this impossible event happens.

## Not according to specification
If sensor b is activated, the barriers have to be going up or be already up within
the current cycle of the loop.

`G( b -> ( (b && !endloop) U ( (N going up || N up) && (S going up || S up) ) ) )`

**Problem:**
This is not according to the specification. 