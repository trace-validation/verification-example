# Verification Example
This project shows how to use the tracing tools ([Requirement Converter](https://gitlab.com/trace-validation/requirement-converter) and [Trace Validator](https://gitlab.com/trace-validation/trace-validator)) to verify requirement on a real program.

## Requirements
* JDK (developed for `8.0`)
* C compiler (e.g. GCC)
* GNU Make
* Apache Maven
* Bash
* `sed`

## Setup
1. `git clone https://gitlab.com/trace-validation/verification-example`
2. `cd verification-example`
3. `git submodule init`
4. `git submodule update`

## 1. Requirement conversion
`requirements/` contains several requirements. The script `convert_requirements.sh` will generate Buchi automata for these.
The resulting automata will also be in the `requirements/` directory, with the `.nba.json` file extension.
The 'correct' output can be found both in `visualisation/` and `validation/requirements/`.

## 2. Visualisation
`visualisation/` contains automata for requirements of `requirements/`. The `visualise.sh` script will generate DOT files. These can be viewed, for example, at [WebGraphviz](http://webgraphviz.com/).
The 'correct' output can be found in `visualisation/target-output/`.

Note that some automata are extremely large (to visualise). The files `barriers-down.nba.json.gv`, `go-signal.nba.json.gv` and `one-train.nba.json.gv` seems to generate the most reasonable graphs.

## 3. Trace generation
The `generate_traces.sh` script will generate a set of traces. The script specifies a set of parameters that are varied to obtain different traces. Output will be generated in `toy-example/traces/`.
The 'correct' output can be found in `validation/traces/`.

Note that this a real-time simulation, so generating traces takes time (roughly half an hour).

## 4. Verification
The verification script called `validate.sh` will pairwise check `validation/requirements/` and `validation/traces/`. Output is written to the terminal.

Especially undecidability may take significant time, depending on the length of the trace. Satisfaction or violation may be faster as it may not be necessary to read the full trace.
